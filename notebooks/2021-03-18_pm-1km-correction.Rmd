---
title: "Correction to 1 km PM model"
output:
  html_notebook:
    code_folding: hide
---

```{r setup}
knitr::opts_chunk$set(fig.height = 6, fig.width = 9.7)
source(here::here("init.R"))

```

```{r load-data}
# Original 1 km predictions
old <- glue("~/summer/modeles_expo/pm_hough_1km/stats/pm_1km_{ALL_YEARS}_smry.fst") %>%
       map(read_fst, as.data.table = T) %>%
       rbindlist() %>%
       setkeyv(c("pol", "sinu_id", "month"))

# Corrected 1 km predictions
new <- here(glue("work/preds/pm_1km_{ALL_YEARS}_smry.fst")) %>%
       map(read_fst, as.data.table = T) %>%
       rbindlist() %>%
       setkeyv(c("pol", "sinu_id", "month"))

# Paranoia
stopifnot(all.equal(old[, 1:3], new[, 1:3]))

# Combine old and new into a single table
new[, min_old  := old$min]
new[, mean_old := old$mean]
new[, max_old  := old$max]
rm(old)

# Overall per-cell min, mean, and max before + after
grd <- read_fst(here("work/geo/grd_1km.fst"), as.data.table = TRUE) %>%
       dplyr::select(starts_with("sinu_"))
percell <- new[, .(min = min(min), mean = mean(mean), max = max(max), min_old = min(min_old),
                   mean_old = mean(mean_old), max_old = max(max_old)),
               keyby = .(pol, sinu_id)] %>%
           .[grd, on = .(sinu_id)]

```

An error in the code caused the 1 km GAM ensemble to give too much weight to the random forest predictions.

Due to the error, the final GAM was trained using in-sample stage 3 predictions (stage 3 predictions for the same monitors that were used to train the stage 3 learners). This resulted in a higher weight for the random forest, because the random forest closely reproduces PM at training monitors. But the random forest overfits: it performs less well at held-out test monitors.

After correcting the error, the 1 km GAM ensemble is trained using out-of-sample stage 3 predictions (stage 3 predictions for monitors that were held out when training the stage 3 learners). This adds regularization: the weights for the stage 3 learners are based on the stage 3 learners' performance at held-out test monitors. This results in a higher weight for the GMRF, which performs well at held-out test monitors. The resulting GAM ensemble performs better at held-out test monitors than the erroneous GAM:

|                       | R^2^ | MAE |
|-----------------------|------|-----|
| PM~2.5~ original GAM  | 0.75 | 3.1 |
| PM~2.5~ corrected GAM | 0.80 | 2.7 |
|                       |      |     |
| PM~10~ original GAM   | 0.69 | 4.8 |
| PM~10~ corrected GAM  | 0.75 | 4.2 |

After correcting the error, the final 1 km predictions will be slightly less accurate at monitor locations, but they should be more accurate at locations where there are no PM monitors.


### PM~2.5~ predictions before vs. after correcting error

After correcting the error, new min, mean, and max PM~2.5~ prediction for each cell (bottom row) is similar to the old predictions (top row), but tends to be lower in rural areas, giving greater contrast between urban and rural areas.

```{r change-dist}
# PM2.5 per-cell min, mean, max before + after
rst <- dcast(percell, sinu_x + sinu_y ~ pol,
             value.var = c("min", "mean", "max", "min_old", "mean_old", "max_old")) %>%
       sinu_to_raster()

plot_fun <- function(x) {
  rasterVis::levelplot(x, cuts = 50, scales = list(draw = FALSE), margin = FALSE,
                       par.settings = rasterVis::infernoTheme(), layout = c(1, 2))
}
print(plot_fun(rst[[c("min_old_PM2.5", "min_PM2.5")]]), split = c(1, 1, 3, 1), more = TRUE)
print(plot_fun(rst[[c("mean_old_PM2.5", "mean_PM2.5")]]), split = c(2, 1, 3, 1), more = TRUE)
print(plot_fun(rst[[c("max_old_PM2.5", "max_PM2.5")]]), split = c(3, 1, 3, 1), more = TRUE)

```


### PM~10~ predictions before vs. after correcting error

As with PM~2.5~, the new min, mean, and max PM~10~ prediction for each cell (bottom row) tends to be lower in rural areas, giving greater contrast between urban and rural areas.

```{r}
# PM10 per-cell min, mean, max before + after
print(plot_fun(rst[[c("min_old_PM10", "min_PM10")]]), split = c(1, 1, 3, 1), more = TRUE)
print(plot_fun(rst[[c("mean_old_PM10", "mean_PM10")]]), split = c(2, 1, 3, 1), more = TRUE)
print(plot_fun(rst[[c("max_old_PM10", "max_PM10")]]), split = c(3, 1, 3, 1), more = TRUE)

```


### Distribution of differences in per-cell min, mean, and max

For most grid cells, the min and mean PM values decreased slightly. There are ocassionally large changes in the maximum value of a grid cell, but these are rare.

```{r}
set.seed(100)
new[sample(.N, 1e7), ] %>% 
  .[, .(pol, diff_in_min = min - min_old, diff_in_mean = mean - mean_old,
        diff_in_max = max - max_old)] %>% 
  melt(id.vars = "pol", variable.name = "stat") %>% 
  ggplot() +
    geom_violin(aes(x = value, y = stat, fill = stat), show.legend = FALSE) +
    facet_grid(cols = vars(pol), scales = "free", space = "free") +
    scale_x_continuous(n.breaks = 15) +
    labs(x = element_blank(), y = element_blank(),
         title = "Distribution of change in per-cell min, mean, and max PM") +
    theme_bw()

```


### Example of mean PM~2.5~ in May 2005

After correcting the error, we no longer see a phenomenon that sometimes ocurred in early years where predicted PM was lower in small cities and the suburbs of large cities than in the surrounding countryside.

```{r}
new[pol == "PM2.5" & month == "2005-05-01", ] %>%
  .[grd, on = .(sinu_id)] %>% # add coords
  melt(measure.vars = c("mean_old", "mean"), variable.name = "version", value.name = "PM2.5") %>%
  .[, version := recode_factor(version, mean_old = "Original", mean = "Corrected")] %>%
  ggplot() +
    geom_tile(aes(x = sinu_x, y = sinu_y, fill = PM2.5)) +
    facet_wrap(vars(version)) +
    scale_fill_viridis_c(option = "inferno") +
    coord_sf(crs = MODIS_SINU, datum = 4326) +
    labs(x = element_blank(), y = element_blank(), fill = expression(PM[2.5]),
         title = expression("Mean"~PM[2.5]~"in May 2005 before/after correcting error")) +
    theme_bw()

```
