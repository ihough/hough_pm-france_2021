# TODO

* run 200 m stage for 2000-2019


# Stage 1: Predict PM2.5 at PM10 stations

## Stage 1 dbs

[x] Clean PM10
[x] Clean PM2.5
[ ] Adjust old observations to account for semi-volatile fraction of PM
    We don't do this for now; look at whether there's a discontinuity in PM predictions at the time of switch and if so reconsider
[ ] DB
    [x] PM2.5 mean
    [x] PM2.5 n hours
    [x] PM10 mean
    [x] PM10 n hours
    [x] lat
    [x] lon
    [x] impl
    [x] infl
    [ ] dow
    [ ] doy
    [x] msr_code
[ ] Repeat for all years

## Stage 1 model

[ ] Tune random forest
[ ] Cross-validate
[ ] Train + predict
[ ] Assess performance, feature importance, feature correlation
[ ] Repeat for all years


# Stage 2: Gapfill MAIAC AOD

## Stage 2 dbs

[x] Clean MAIAC AOD
[x] Extract CAMSRA AOD
[ ] Extract MERRA AOD (2000 - 2002)
[x] DBs
    [x] sinu_id
    [x] sinu_x
    [x] sinu_y
    [x] aod_047 (MAIAC)
    [x] dow
    [x] doy
    [x] time
    [x] maiac_n
    [x] aod469_utc09 (CAMSRA)
    [x] aod469_utc10 (CAMSRA)
    [x] aod469_utc11 (CAMSRA)
    [x] aod469_utc12 (CAMSRA)
    [x] aod469_utc13 (CAMSRA)
    [x] aod469_utc14 (CAMSRA)
    [x] aod469_utc15 (CAMSRA)
[ ] Repeat for all years

## Stage 2 model

[x] Tune random forest
[x] Cross-validate
[x] Train + predict
[ ] Assess performance, feature importance, feature correlation
[ ] Repeat for all years


# Stage 3: Model PM at 1 km

## Stage 3 dbs

[ ] DBs
   [ ] PM10
   [ ] PM2.5
   [ ] PM modeled flag
   [ ] PM n hours
   [ ] PM stn id
   [ ] aod_047
   [ ] aod modeled flag
   [ ] aod_time
   [ ] maiac_n
   [ ] sinu_x
   [ ] sinu_y
   [ ] dow
   [ ] sin((dow - 1) / 7 * pi)
   [ ] doy
   [ ] sin((doy - 1) / 365.25 * pi)
   [x] ECMWF meteo
       [x] PBL height 0 UTC
       [x] PBL height 12 UTC
       [x] Ta mean
       [x] Ta sd
       [x] Tdew mean (humidity not available in ERA5 surface data)
       [x] Surface pressure mean
       [x] Precip sum
       [x] Wind U mean (transform?)
       [x] Wind V mean (transform?)
       [x] Cloud cover mean
   [ ] elevation
   [ ] NDVI (monthly)
   [ ] population
   [ ] highway density
   [ ] highway dist
   [ ] major road density
   [ ] major road dist
   [ ] local road density
   [ ] local road dist
   [ ] rail density
   [ ] rail dist
   [ ] airport dist
   [ ] port dist
   [ ] major emitter dist
   [ ] 2010 annual emissions
   [ ] coast dist
   [ ] lake dist
   [ ] impervious surface (%)
   [ ] land cover (% industrial)
   [ ] land cover (% urban dense)
   [ ] land cover (% urban sparse)
   [ ] land cover (% arable)
   [ ] land cover (% forest)
   [ ] land cover (% open)
[ ] Repeat for all years


## Stage 3 model

[ ] Tune random forest
[ ] Cross-validate RF
[ ] Train + predict RF
[ ] Tune GMRF
[ ] Cross-validate GMRF
[ ] Train + predict GMRF
[ ] Tune ensemble
[ ] Cross-validate ensemble
[ ] Train + predict ensemble
[ ] Assess performance, feature importance, feature correlation (RF, GMRF, ensemble)
[ ] Repeat for all years


# Stage 4: Model PM at 200 m

## Stage 4 dbs

[ ] DBs
[ ] Repeat for all years


## Stage 4 model

[ ] Tune random forest
[ ] Cross-validate RF
[ ] Train + predict RF
[ ] Tune GMRF
[ ] Cross-validate GMRF
[ ] Train + predict GMRF
[ ] Assess performance, feature importance, feature correlation
[ ] Repeat for all years


# Questions / possible improvments

* Cyclic variable (sin / cos) of day of week, day of year? include both sin and cos? but then RF will randomly choose 1...
