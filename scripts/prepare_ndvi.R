#
# Functions to read and quality-control MODIS NDVI
#
# 1. Read subdatasets from HDF files
# 2. Rescale NDVI (required due to error in metadata)
# 3. Parse QA codes
# 4. Save to one fst file per month
#
# TODO potential improvements
# * Explore which QA codes should be included / excluded
#   - Load all data for a year
#   - Map missingness for all year and by season with different combos of QA codes
#


# Read a single subdataset from a MODIS hdf file
# -> RasterLayer
modis_read_sd <- function(sd_path, rescale = NULL, verbose = FALSE) {

  # Parse the subdataset name from the path
  sd_full <- str_split_fixed(sd_path, "\\.hdf:", 2)[2]
  sd_name <- str_extract(sd_full, "\\w+$")

  report(glue("{sd_full}"), verbose)

  # Load the data as a RasterLayer and name
  rst <- withCallingHandlers(
           raster::raster(sd_path) %>%
           magrittr::set_names(sd_name),
           warning = muffle_crs_warnings
         )

  # Possibly apply a custom scale factor
  # e.g. NDVI metadata incorrectly lists scale factor as 10000 instead of 0.0001; fix this by
  # using scale_fact = (1/10000)^2
  if (!is.null(rescale)) {

    rst <- rst * rescale

  }

  return(rst)

}

# Load subdatasets from a MODIS hdf file
modis_read_hdf <- function(hdf_path, sds = NULL, verbose = FALSE) {

  # If sds is a list separate it into subdataset names and rescaling factors
  if (is.list(sds)) {
    rescales <- purrr::map(sds, "rescale") # return list so it can include NULL
    sds <- purrr::map_chr(sds, "name")
  }

  report(glue("Reading {basename(hdf_path)}"), verbose)

  # List the path to every subdataset in the file
  # FOR CIMENT: clean the paths by removing double quotes (")
  sd_paths <- sf::gdal_subdatasets(hdf_path) %>%
              gsub('"', "", .)

  # Select the datasets to load
  if (!is.null(sds)) {

    # Note: this raises if any sds not found
    sd_paths <- paste0(sds, "$") %>%
                purrr::map_chr(~ str_subset(sd_paths, .x))

  }

  # Extract each subdataset as a RasterLayer, possibly rescaling
  if (exists("rescales")) {

    dat <- purrr::map2(.x = sd_paths, .y = rescales, ~ modis_read_sd(sd_path = .x, rescale = .y))

  } else {

    dat <- purrr::map(sd_paths, modis_read_sd)

  }

  # Stack all layers and convert to data.frame
  dat %<>% raster::stack() %>%
           raster::as.data.frame(xy = TRUE)

  return(dat)

}

# Convert an NDVI QA code from an integer to a character vector showing bits
# e.g. 2116 -> "0 0 001 0 0 0 01 0001 00" i.e. good quality, low aerosol, no cloud, land
modis_ndvi_qa_bits <- function(x) {

  parse_qa <- function(x) {

    bits <- intToBits(x) %>%
            as.integer
    paste(
      paste(bits[16], collapse = ""),    # shadow
      paste(bits[15], collapse = ""),    # snow / ice
      paste(bits[14:12], collapse = ""), # land / water
      paste(bits[11], collapse = ""),    # mixed clouds
      paste(bits[10], collapse = ""),    # atmosphere BRDF correction
      paste(bits[9], collapse = ""),     # adjacent cloud
      paste(bits[8:7], collapse = ""),   # aerosol quantity
      paste(bits[6:3], collapse = ""),   # usefulness
      paste(bits[2:1], collapse = "")    # quality
    )

  }

  purrr::map_chr(x, parse_qa)

}

# Extract the usefulness score from an NDVI QA code
# -> integer from 0 (highest quality) to 12 (lowest quality)
modis_ndvi_usefulness <- function(qa_code) {

  bitwAnd(bitwShiftR(qa_code, 2), strtoi("1111", base = 2))

}

# Format MODIS NDVI
# * Drop rows without NDVI, optionally crop, set usefulness score based on QA bits
modis_format_ndvi <- function(dat, bbox = NULL) {

  # Ensure data.table
  setDT(dat)

  # Drop rows missing NDVI
  dat %<>% .[!is.na(NDVI), ]

  # Crop by bounding box if provided
  if (!missing(bbox)) {
    dat %<>% .[x %between% bbox$xlim & y %between% bbox$ylim, ]
  }

  # Convert the QA usefulness bits to an integer from 0 (highest quality) to 12 (lowest quality)
  dat[, Usefulness := modis_ndvi_usefulness(Quality)]

  # Add an ID hashed from the X and Y coordinates
  dat[, sinu_id  := hash_sinu(x, y)]

  # Rename and order columns
  dat %>%
    setnames(c("x", "y"), c("sinu_x", "sinu_y")) %>%
    setcolorder(c("sinu_id", "sinu_x", "sinu_y"))

}

# Prepre MODIS Terra monthly NDVI
# ~ 30 seconds
# [ varying x 6 ]
# [ sinu_id sinu_x sinu_y NDVI Quality Usefulness ]
prepare_ndvi_modis <- function(year, res = c("1km", "250m"), data_dir, ncores = 1L, force = FALSE,
                               verbose = FALSE) {

  res <- match.arg(res) # prepare 1 km or 250 m NDVI data

  # Skip if already processed
  ndvi_paths <- lubridate::make_date(year, study_months(year), 1) %>%
                purrr::set_names() %>%
                purrr::map_chr(function(day) {
                  here(glue("work/data/{year}/ndvi_terra_{res}_{format(day, '%Y-%m')}.fst"))
                })
  if (all_exist(ndvi_paths, force = force)) return(as.character(ndvi_paths))

  report(glue("[prepare_ndvi_modis] {year}: preparing MODIS Terra NDVI"))

  # Define the dataset to extract
  if (res == "1km") {

    dataset <- "MOD13A3"
    sds_prefix <- "1 km monthly"

  } else {

    dataset <- "MOD13Q1"
    sds_prefix <- "250m 16 days"

  }
  src_dir <- glue("{data_dir}/ndvi/{dataset}.006/raw/{year}")

  # Define the subdatasets to extract
  # NDVI data must be rescaled to fix metadata error
  sds = list(
          list(name = glue("{sds_prefix} NDVI"), rescale = 1/10000^2),
          # list(name = glue("{sds_prefix} EVI")),
          list(name = glue("{sds_prefix} VI Quality"))
          # list(name = glue("{sds_prefix} red reflectance")),
          # list(name = glue("{sds_prefix} NIR reflectance")),
          # list(name = glue("{sds_prefix} blue reflectance")),
          # list(name = glue("{sds_prefix} MIR reflectance")),
          # list(name = glue("{sds_prefix} view zenith angle")),
          # list(name = glue("{sds_prefix} sun zenith angle")),
          # list(name = glue("{sds_prefix} relative azimuth angle")),
          # list(name = glue("{sds_prefix} pixel reliability"))
        )

  # Process missing months in parallel (unless forcing)
  fsts <- path_missing(ndvi_paths, force = force) %>%
          names() %>%
          mc_safely(function(day) {

            ndvi_path <- ndvi_paths[day]
            day <- lubridate::as_date(day)
            ym <- format(day, "%Y-%m")

            msg_prefix <- glue("[prepare_ndvi_modis] {ym}")

            # Warn if there are not 3 HDFS for each timepoint
            hdfs <- glue("{dataset}\\.A{year}\\d{{3}}.+\\.hdf$") %>%
                    list.files(src_dir, pattern = ., full.names = TRUE) %>%
                    data.table(path = .) %>%
                    .[, date := str_extract(path, "(?<=\\.A)\\d{7}(?=\\.)") %>%
                                lubridate::parse_date_time("Yj")] %>%
                    .[month(date) == month(day), ] %>%
                    .[, count := .N, by = .(date)]
            wrong <- hdfs[count != 3, ]
            if (nrow(wrong) > 0) {
              msg <- glue("{msg_prefix}: {nrow(hdfs)} HDFs (expected 3)")
              report(msg)
              report(wrong$hdfs)
              warning(msg)
            }

            # Define the bounds of the study area (pixels outside this will be cropped)
            bbox <- withCallingHandlers(
                      here("work/geo/grd_1km_rst.tif") %>%
                        raster::raster() %>%
                        raster::extent() %>%
                        raster::extend(5000) %>% # add a 5 km buffer on all sides
                        st_bbox(),
                      warning = muffle_crs_warnings
                    )

            # Load, clean, and combine data
            ndvi <- purrr::map(hdfs$path, function(hdf) {

                      ndvi <- modis_read_hdf(hdf, sds = sds, verbose = verbose) %>%
                              modis_format_ndvi(bbox = bbox)
                      return(ndvi)

                    })
            ndvi %<>% rbindlist()
            names(ndvi) %>% paste(collapse = " ")
            # sinu_id sinu_x sinu_y NDVI Quality Usefulness

            # If NDVI data is 16-day, composite the maximum value at each location across all
            # timepoints in the month
            if (dataset == "MOD13Q1") {
              ndvi %<>% .[order(-NDVI), lapply(.SD, first), keyby = .(sinu_id)]
            }

            # Key and save to disk
            setkey(ndvi, "sinu_id")
            save_safely(ndvi, write_fst, ndvi_path, compress = 100)
            report(glue("{msg_prefix}: -> {ndvi_path}"))

            return(ndvi_path)

          }, mc.cores = ncores, mc.preschedule = FALSE)
  fsts %<>% unlist(use.names = FALSE)

  # Report new files (on some machines messages from within mclapply do not display)
  report(glue("[prepare_ndvi_modis] {year}: -> {fsts}"))

  # Return paths to monthly data
  return(as.character(ndvi_paths))

}
