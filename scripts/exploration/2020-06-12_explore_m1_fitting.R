source("init.R")

ncores <- get_ncores()
verbose <- TRUE


# Setup -----

# Load db1 and restrict to simultaneous observations of both PM10 and PM2.5
db1_cc <- here(glue("work/db/db1_2000-2018.fst")) %>%
          read_fst(as.data.table = TRUE) %>%
          .[!is.na(PM10) & !is.na(PM2.5), ]

# Format columns (mlr can't handle Date or character columns)
# * Add an integer date index (date as number of days since 1970-01-01)
# * Convert impl and infl to factors
# * Recode impl to combine all rural implantations (needed b/c no R stations measure PM2.5)
db1_cc[, t_idx := as.integer(date)]
db1_cc[, impl := recode(impl, RN = "RA", RR = "RA", RP = "RA", R = "RA") %>%
                 factor(levels = c("U", "PU", "RA"))]
db1_cc[, infl := factor(infl, levels = c("T", "F", "I"))]


# Explore tuning PM2.5 -----

pol <- "PM2.5"
obs_pol <- "PM10"

# Define the modelling task, resampling strategy, and performance measures
# * Include msr_code as feature
# * 5-fold CV with blocking by station
# * Random forest sampling without replacement (better for variable importance)
report(glue("preparing task"), verbose)
features <- c(obs_pol, glue("msr_code_{obs_pol}"), "lat", "lon", "impl", "infl", "dow", "doy",
              "t_idx")
task <- mlr::makeRegrTask(
          id = glue("m1_{pol}_explore"),
          data = db1_cc[, c(pol, features), with = FALSE] %>%
                 as.data.frame(),
          target = pol,
          blocking = as.factor(db1_cc$stn_code)
        )
resamp <- mlr::makeResampleDesc(method = "CV", iters = 5, blocking.cv = TRUE)
measures <- list(mlr::rsq, mlr::mae, mlr::rmse, mlr::timetrain)
learner = mlr::makeLearner(
            cl = "regr.ranger",
            num.threads = ncores,
            replace = FALSE
          )

dir.create(here("work/explore/m1"), recursive = TRUE, showWarnings = FALSE)

# 1. What is best mtry? (for subset of 100k rows, 100 trees) -----
n_features <- mlr::getTaskNFeats(task)
tune1 <- mlr::tuneParams(
           learner = mlr::setHyperPars(learner, num.trees = 100),
           task = mlr::subsetTask(task, sample(mlr::getTaskSize(task), 100000)),
           resampling = resamp,
           measures = measures,
           par.set = makeParamSet(makeIntegerParam("mtry", 1, n_features)),
           control = mlr::makeTuneControlMBO(
                       budget = n_features + 1,
                       mbo.design = data.frame(mtry = seq(2, n_features, by = 2))
                     ),
           show.info = TRUE
         )
saveRDS(tune1, here("work/explore/m1/m1_pm25_tune1.rds"))

mlr::getTuneResultOptPath(tune1)
# mtry rsq.test.mean mae.test.mean rmse.test.rmse timetrain.test.mean dob exec.time
#    2     0.8484895      2.784656       3.904852              0.6944   1     5.024
#    4     0.8588305      2.648982       3.767853              1.0930   2     7.365
#    6     0.8546662      2.680824       3.823086              1.4352   3     8.920
#    8     0.8503762      2.713120       3.878845              1.5552   4     9.786
#    3     0.8598798      2.647225       3.754270              0.9740   5     7.157
#    3     0.8601144      2.645047       3.751353              0.8682   6     5.992
#    3     0.8599791      2.645940       3.752996              0.8490   7     5.781
#    3     0.8598680      2.647770       3.754535              0.8588   8     6.461
#    3     0.8600466      2.643847       3.752085              0.8712   9     5.942
#    3     0.8601464      2.644654       3.750507              0.9220  10     6.608

# Plot R2
mlr::generateHyperParsEffectData(tune1)$data %>%
  ggplot() +
    geom_point(aes(x = mtry, y = rsq.test.mean))

# Plot MAE
mlr::generateHyperParsEffectData(tune1)$data %>%
  ggplot() +
    geom_point(aes(x = mtry, y = mae.test.mean))

# Conclusion: mtry = 3 is best; 4 might be ok


# 2. Does increasing sample.fraction substantially improve performance? -----
tune2 <- mlr::tuneParams(
           learner = mlr::setHyperPars(learner, num.trees = 100),
           task = mlr::subsetTask(task, sample(mlr::getTaskSize(task), 100000)),
           resampling = resamp,
           measures = measures,
           par.set = makeParamSet(
                       makeIntegerParam("mtry", 3, 5),
                       makeNumericParam("sample.fraction", 0.6, 0.95)
                     ),
           control = mlr::makeTuneControlMBO(
                       budget = 16,
                       mbo.design = expand.grid(
                                      mtry = 3:5,
                                      sample.fraction = seq(0.6, 0.95, length.out = 4)
                                    )
                     ),
           show.info = TRUE
         )
saveRDS(tune2, here("work/explore/m1/m1_pm25_tune2.rds"))

mlr::getTuneResultOptPath(tune2)
# mtry sample.fraction rsq.test.mean mae.test.mean rmse.test.rmse timetrain.test.mean dob exec.time
#    3       0.6000000     0.8574322      2.672781       3.800462              0.9668   1     6.347
#    4       0.6000000     0.8566012      2.672814       3.812016              0.9768   2     6.458
#    5       0.6000000     0.8552860      2.681697       3.829979              1.1082   3     7.240
#    3       0.7166667     0.8579703      2.664977       3.792909              0.9790   4     6.650
#    4       0.7166667     0.8569959      2.669099       3.806741              1.1680   5     7.632
#    5       0.7166667     0.8546949      2.687485       3.837861              1.2106   6     7.794
#    3       0.8333333     0.8582916      2.661790       3.788514              0.9746   7     7.125
#    4       0.8333333     0.8562472      2.671738       3.817012              1.1976   8     7.747
#    5       0.8333333     0.8540658      2.690513       3.845913              1.4162   9     8.813
#    3       0.9500000     0.8580611      2.661261       3.791707              1.3248  10     8.480
#    4       0.9500000     0.8567436      2.668388       3.810163              1.4320  11     9.059
#    5       0.9500000     0.8534835      2.695227       3.853669              1.6072  12     9.987
#    3       0.8724084     0.8584397      2.658605       3.786496              1.0776  13     7.161
#    3       0.8701863     0.8577541      2.663953       3.796440              0.9268  14     6.908
#    3       0.9500000     0.8587347      2.655908       3.782924              1.2616  15     8.180
#    3       0.9499980     0.8588148      2.655945       3.782050              1.2768  16     8.349

# Plot R2
mlr::generateHyperParsEffectData(tune2)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.rsq = mean(rsq.test.mean)) %>%
  ggplot(aes(x = sample.fraction, y = rsq.test.mean, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.rsq, color = as.factor(mtry)), linetype = "dotted")

# Plot MAE
mlr::generateHyperParsEffectData(tune2)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.mae = mean(mae.test.mean)) %>%
  ggplot(aes(x = sample.fraction, y = mae.test.mean, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.mae, color = as.factor(mtry)), linetype = "dotted")

# Plot RMSE
mlr::generateHyperParsEffectData(tune2)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.rmse = mean(rmse.test.rmse)) %>%
  ggplot(aes(x = sample.fraction, y = rmse.test.rmse, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.rmse, color = as.factor(mtry)), linetype = "dotted")

# Conclusion: increasing sample.fraction barely improves performance (R2 +0.001, MAE -0.015)


# 3. Do results hold for larger sample and more trees? -----
tune3 <- mlr::tuneParams(
           learner = mlr::setHyperPars(learner, num.trees = 300),
           task = mlr::subsetTask(task, sample(mlr::getTaskSize(task), 300000)),
           resampling = resamp,
           measures = measures,
           par.set = makeParamSet(
                       makeIntegerParam("mtry", 2, 5),
                       makeNumericParam("sample.fraction", 0.6, 0.95)
                     ),
           control = mlr::makeTuneControlMBO(
                       budget = 20,
                       mbo.design = expand.grid(
                                      sample.fraction = seq(0.6, 0.95, length.out = 4),
                                      mtry = 3:5
                                    )
                     ),
           show.info = TRUE
         )
saveRDS(tune3, here("work/explore/m1/m1_pm25_tune3.rds"))

mlr::getTuneResultOptPath(tune3)
# mtry sample.fraction rsq.test.mean mae.test.mean rmse.test.rmse timetrain.test.mean dob exec.time
#    3       0.6000000     0.8669416      2.580097       3.669757              8.9714   1    52.174
#    3       0.7166667     0.8671848      2.576970       3.666058              9.5364   2    56.149
#    3       0.8333333     0.8673811      2.574016       3.663378              9.8952   3    59.053
#    3       0.9500000     0.8676052      2.569471       3.660260             10.5744   4    63.243
#    4       0.6000000     0.8648333      2.593373       3.698169             10.1328   5    58.040
#    4       0.7166667     0.8651191      2.589369       3.694022             11.1528   6    65.001
#    4       0.8333333     0.8652267      2.585934       3.692382             12.1740   7    71.091
#    4       0.9500000     0.8650337      2.585538       3.695035             12.7322   8    75.428
#    5       0.6000000     0.8627015      2.608850       3.727288             11.3854   9    64.008
#    5       0.7166667     0.8627207      2.606669       3.726747             12.7542  10    72.853
#    5       0.8333333     0.8623181      2.609355       3.732418             13.5618  11    78.141
#    5       0.9500000     0.8614330      2.614611       3.744100             14.7102  12    84.697
#    2       0.9499999     0.8597217      2.689235       3.770765              6.6562  13    38.895
#    3       0.9462940     0.8677657      2.567951       3.658007             10.5718  14    62.936
#    3       0.9499992     0.8675432      2.571408       3.661037             10.4838  15    62.343
#    3       0.9499990     0.8674661      2.572211       3.662212             10.9342  16    64.717
#    3       0.9381644     0.8675660      2.569407       3.660495             10.0802  17    60.610
#    3       0.9499998     0.8676234      2.569231       3.659859             10.8754  18    64.461
#    3       0.9500000     0.8676192      2.569722       3.659572             10.5272  19    63.626
#    3       0.9499981     0.8672704      2.572443       3.664758             10.4498  20    62.398

# Plot R2
mlr::generateHyperParsEffectData(tune3)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.rsq = mean(rsq.test.mean)) %>%
  ggplot(aes(x = sample.fraction, y = rsq.test.mean, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.rsq, color = as.factor(mtry)), linetype = "dotted")

# Plot MAE
mlr::generateHyperParsEffectData(tune3)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.mae = mean(mae.test.mean)) %>%
  ggplot(aes(x = sample.fraction, y = mae.test.mean, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.mae, color = as.factor(mtry)), linetype = "dotted")

# Plot RMSE
mlr::generateHyperParsEffectData(tune3)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.rmse = mean(rmse.test.rmse)) %>%
  ggplot(aes(x = sample.fraction, y = rmse.test.rmse, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.rmse, color = as.factor(mtry)), linetype = "dotted")

# Conclusion:
#   mtry = 3 is best; 4 is ok
#   sample.size does not substantially improve performance (R2 +0.0005, MAE -0.01)





# Explore tuning PM10 -----

pol <- "PM10"
obs_pol <- "PM2.5"

report(glue("preparing task"), verbose)
features <- c(obs_pol, glue("msr_code_{obs_pol}"), "lat", "lon", "impl", "infl", "dow", "doy",
              "t_idx")
task <- mlr::makeRegrTask(
          id = glue("m1_{pol}_explore"),
          data = db1_cc[, c(pol, features), with = FALSE] %>%
                 as.data.frame(),
          target = pol,
          blocking = as.factor(db1_cc$stn_code)
        )
resamp <- mlr::makeResampleDesc(method = "CV", iters = 5, blocking.cv = TRUE)
measures <- list(mlr::rsq, mlr::mae, mlr::rmse, mlr::timetrain)
learner = mlr::makeLearner(
            cl = "regr.ranger",
            num.threads = ncores,
            replace = FALSE
          )

dir.create(here("work/explore/m1"), recursive = TRUE, showWarnings = FALSE)

# 1. What is best mtry? (for subset of 100k rows, 100 trees) -----
n_features <- mlr::getTaskNFeats(task)
tune1 <- mlr::tuneParams(
           learner = mlr::setHyperPars(learner, num.trees = 100),
           task = mlr::subsetTask(task, sample(mlr::getTaskSize(task), 100000)),
           resampling = resamp,
           measures = measures,
           par.set = makeParamSet(makeIntegerParam("mtry", 1, n_features)),
           control = mlr::makeTuneControlMBO(
                       budget = n_features + 1,
                       mbo.design = data.frame(mtry = seq(2, n_features, by = 2))
                     ),
           show.info = TRUE
         )
saveRDS(tune1, here("work/explore/m1/m1_pm10_tune1.rds"))

mlr::getTuneResultOptPath(tune1)
# mtry rsq.test.mean mae.test.mean rmse.test.rmse timetrain.test.mean dob exec.time
#    2     0.8097275      3.892416       5.540543              0.6134   1     4.476
#    4     0.8275128      3.731122       5.265185              1.2176   2     7.692
#    6     0.8262769      3.752698       5.285040              1.5392   3     9.253
#    8     0.8226901      3.790191       5.339737              1.6508   4     9.846
#    6     0.8258355      3.755590       5.291831              1.5244   5     9.224
#    3     0.8277488      3.711910       5.264409              1.1516   6     7.224
#    5     0.8271598      3.737867       5.271218              1.3966   7     8.525
#    6     0.8258518      3.757093       5.291367              1.4384   8     8.798
#    7     0.8234101      3.782631       5.329064              1.6312   9     9.794
#    1     0.5950205      5.724039       8.094606              0.4146  10     3.067

# Plot R2
mlr::generateHyperParsEffectData(tune1)$data %>%
  ggplot() +
    geom_point(aes(x = mtry, y = rsq.test.mean))

# Plot MAE
mlr::generateHyperParsEffectData(tune1)$data %>%
  ggplot() +
    geom_point(aes(x = mtry, y = mae.test.mean))

# Conclusion: mtry = 3 is best; 4 or 5 might be ok


# 2. Does increasing sample.fraction substantially improve performance? -----
tune2 <- mlr::tuneParams(
           learner = mlr::setHyperPars(learner, num.trees = 100),
           task = mlr::subsetTask(task, sample(mlr::getTaskSize(task), 100000)),
           resampling = resamp,
           measures = measures,
           par.set = makeParamSet(
                       makeIntegerParam("mtry", 3, 5),
                       makeNumericParam("sample.fraction", 0.6, 0.95)
                     ),
           control = mlr::makeTuneControlMBO(
                       budget = 16,
                       mbo.design = expand.grid(
                                      mtry = 3:5,
                                      sample.fraction = seq(0.6, 0.95, length.out = 4)
                                    )
                     ),
           show.info = TRUE
         )
saveRDS(tune2, here("work/explore/m1/m1_pm10_tune2.rds"))

mlr::getTuneResultOptPath(tune2)
# mtry sample.fraction rsq.test.mean mae.test.mean rmse.test.rmse timetrain.test.mean dob exec.time
#    3       0.6000000     0.8306006      3.721218       5.264895              0.9116   1     6.018
#    4       0.6000000     0.8303892      3.737102       5.266640              1.0764   2     6.977
#    5       0.6000000     0.8291931      3.753040       5.283357              1.0524   3     7.208
#    3       0.7166667     0.8315970      3.715103       5.248077              0.8950   4     6.721
#    4       0.7166667     0.8305164      3.735352       5.263204              1.1474   5     7.286
#    5       0.7166667     0.8276076      3.768677       5.307149              1.4940   6     9.146
#    3       0.8333333     0.8316838      3.715941       5.247314              1.1908   7     7.538
#    4       0.8333333     0.8307104      3.736706       5.259817              1.4380   8     8.989
#    5       0.8333333     0.8272262      3.778031       5.313395              1.5648   9     9.728
#    3       0.9500000     0.8322181      3.715591       5.238521              1.3026  10     8.358
#    4       0.9500000     0.8296203      3.746342       5.276895              1.4424  11     9.145
#    5       0.9500000     0.8268401      3.779737       5.318550              1.6448  12    10.108
#    3       0.9499988     0.8313985      3.720416       5.251812              1.2686  13     8.208
#    3       0.9019132     0.8309596      3.725063       5.259975              1.2744  14     8.001
#    3       0.9499991     0.8321412      3.714604       5.238858              1.2966  15     8.172
#    3       0.9499997     0.8315319      3.720990       5.249638              1.1754  16     7.734

# Plot R2
mlr::generateHyperParsEffectData(tune2)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.rsq = mean(rsq.test.mean)) %>%
  ggplot(aes(x = sample.fraction, y = rsq.test.mean, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.rsq, color = as.factor(mtry)), linetype = "dotted")

# Plot MAE
mlr::generateHyperParsEffectData(tune2)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.mae = mean(mae.test.mean)) %>%
  ggplot(aes(x = sample.fraction, y = mae.test.mean, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.mae, color = as.factor(mtry)), linetype = "dotted")

# Plot RMSE
mlr::generateHyperParsEffectData(tune2)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.rmse = mean(rmse.test.rmse)) %>%
  ggplot(aes(x = sample.fraction, y = rmse.test.rmse, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.rmse, color = as.factor(mtry)), linetype = "dotted")

# Conclusion: increasing sample.fraction barely improves performance (R2 +0.001, MAE -0.005)


# 3. Do results hold for larger sample and more trees? -----
tune3 <- mlr::tuneParams(
           learner = mlr::setHyperPars(learner, num.trees = 300),
           task = mlr::subsetTask(task, sample(mlr::getTaskSize(task), 300000)),
           resampling = resamp,
           measures = measures,
           par.set = makeParamSet(
                       makeIntegerParam("mtry", 2, 5),
                       makeNumericParam("sample.fraction", 0.6, 0.95)
                     ),
           control = mlr::makeTuneControlMBO(
                       budget = 20,
                       mbo.design = expand.grid(
                                      sample.fraction = seq(0.6, 0.95, length.out = 4),
                                      mtry = 3:5
                                    )
                     ),
           show.info = TRUE
         )
saveRDS(tune3, here("work/explore/m1/m1_pm10_tune3.rds"))

mlr::getTuneResultOptPath(tune3)
# mtry sample.fraction rsq.test.mean mae.test.mean rmse.test.rmse timetrain.test.mean dob exec.time
#    3       0.6000000     0.8418254      3.597706       5.103339              8.3154   1    49.027
#    3       0.7166667     0.8423282      3.590266       5.095332              9.1522   2    54.265
#    3       0.8333333     0.8423643      3.589439       5.095429              9.7460   3    57.334
#    3       0.9500000     0.8426163      3.586999       5.091364              9.9202   4    59.873
#    4       0.6000000     0.8409714      3.613726       5.117009             10.0016   5    57.086
#    4       0.7166667     0.8410438      3.611623       5.116453             10.6894   6    63.130
#    4       0.8333333     0.8410933      3.612832       5.115552             12.3266   7    71.950
#    4       0.9500000     0.8404001      3.619637       5.126725             12.2882   8    72.824
#    5       0.6000000     0.8389992      3.639893       5.148878             11.0286   9    62.164
#    5       0.7166667     0.8386376      3.643870       5.154677             11.5200  10    66.544
#    5       0.8333333     0.8377736      3.652730       5.168741             12.7172  11    74.451
#    5       0.9500000     0.8371700      3.661035       5.178598             15.3852  12    88.532
#    3       0.9499989     0.8425116      3.588345       5.092649              9.4932  13    56.905
#    3       0.9499982     0.8427812      3.583635       5.088941              9.9904  14    59.368
#    3       0.9499998     0.8425654      3.587786       5.091951             10.5896  15    62.438
#    3       0.9499990     0.8428729      3.585077       5.086917              9.3766  16    56.575
#    3       0.9499965     0.8426696      3.586851       5.090011              9.4662  17    56.708
#    3       0.9499992     0.8423466      3.588894       5.095713             10.1938  18    60.720
#    3       0.9499995     0.8426993      3.586789       5.089832              9.9304  19    59.071
#    3       0.9499996     0.8425179      3.587995       5.092742             10.2532  20    60.566

# Plot R2
mlr::generateHyperParsEffectData(tune3)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.rsq = mean(rsq.test.mean)) %>%
  ggplot(aes(x = sample.fraction, y = rsq.test.mean, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.rsq, color = as.factor(mtry)), linetype = "dotted")

# Plot MAE
mlr::generateHyperParsEffectData(tune3)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.mae = mean(mae.test.mean)) %>%
  ggplot(aes(x = sample.fraction, y = mae.test.mean, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.mae, color = as.factor(mtry)), linetype = "dotted")

# Plot RMSE
mlr::generateHyperParsEffectData(tune3)$data %>%
  dplyr::group_by(mtry) %>%
  dplyr::mutate(mean.rmse = mean(rmse.test.rmse)) %>%
  ggplot(aes(x = sample.fraction, y = rmse.test.rmse, color = as.factor(mtry))) +
    geom_point() +
    geom_line() +
    geom_hline(aes(yintercept = mean.rmse, color = as.factor(mtry)), linetype = "dotted")

# Conclusion:
#   mtry = 3 is best; 4 is ok
#   sample.size does not substantially improve performance (R2 +0.001, MAE -0.01)
