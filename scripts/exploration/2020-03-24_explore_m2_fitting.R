#
# Expermients for m2 = gapfill AOD
#


FORCE <- FALSE
VERBOSE <- TRUE
year <- 2012
samples_to_run <- c("jun", "northeast", "sparse")
exp_dir <- here("work/explore/m2")


source(here::here("init.R"))


report("--- Explore fitting m2 ---")


# Ensure output dir exists
dir.create(exp_dir, recursive = TRUE, showWarnings = FALSE)

# Load data -----
report(glue("Loading {year} db2_cc"), VERBOSE)
db2_cc <- here(glue("work/db/{year}/db2_{year}_cc.fst")) %>%
          read_fst(as.data.table = TRUE)


# Assign each cell-day to a spatially contiguous block for cross-validation -----
m2_block_for_cv <- function(db, centers = 40, verbose = FALSE) {

  report(glue("Creating {centers} spatial CV blocks"), verbose)

  # Cluster cells by spatial coordinates (rounded to 4 km to ensure converges)
  cells <- db[, lapply(.SD, first), .SDcols = c("sinu_x", "sinu_y"), by = .(sinu_id)]
  km <- cells[, .(sinu_x, sinu_y)] %>%
        magrittr::divide_by(4000) %>%
        round() %>%
        kmeans(centers = centers, iter.max = 20)
  if (km$ifault != 0) {
    stop("Kmeans clustering of grid cells did not converge")
  }
  cells[, block := km$cluster]

  # Make each cluster-day a separate CV block
  # This allows the training set to include test set cells on different days
  blocks <- cells[db, .(date, sinu_id, block), on = .(sinu_id)] %>%
            setkeyv(c("date", "sinu_id")) %>%
            .[, block + (100 * as.integer(date))] %>%
            as.factor()
  blocks

}
db2_cc[, cv_block := m2_block_for_cv(db2_cc, verbose = VERBOSE)]


# Make samples -----
report(glue("Creating samples [{paste(samples_to_run, collapse = ' ')}]"), VERBOSE)
samples <- list(

             # Jun ~ 3500000 cell-days
             jun = db2_cc[month(date) == 6, ],

             # Northeast France ~ 3500000 cell-days
             northeast = db2_cc[sinu_x > 350000 & sinu_y > 5290000, ],

             # All observations from 40000 random cells ~ 3500000 cell-days
             sparse = db2_cc[, .(sinu_id = unique(sinu_id))] %>%
                      .[sample(.N, 40000), ] %>%
                      db2_cc[., on = .(sinu_id)]

           )


# Define the learner and performance measures -----
learner <- mlr::makeLearner(
             cl = "regr.ranger",
             importance = "none", # FIXME TESTING NO IMPORTANCE
             replace = FALSE,
             num.threads = NCORES,
             num.trees = 50
           )
trn_n <- mlr::makeMeasure(
           id = "trn_n",
           minimize = F,
           properties = c("regr", "req.pred"),
           fun = function(task, model, pred, feats, extra.args) {
                   getTaskSize(task) - length(getPredictionTruth(pred))
                 },
           aggr = mlr::test.mean,
           best = Inf,
           worst = 0,
           name = "Size of train set"
         )
measures <- list(mlr::rsq, mlr::mae, mlr::timetrain, trn_n)


# Run expermients -----
for (test in samples_to_run) {

  # Define the task
  dat <- samples[[test]]
  features <- c("sinu_x", "sinu_y", "dow", "doy", "aod469_utc09", "aod469_utc12", "aod469_utc15")
              # "aod469_utc10", "aod469_utc11", "aod469_utc13", "aod469_utc14", "time", "maiac_n"
  task <- mlr::makeRegrTask(
            id = glue("{test}"),
            data = dat[, c("aod_047", features), with = FALSE] %>%
                   as.data.frame(),
            target = "aod_047",
            blocking = dat$cv_block
          )


  # 1. Which resampling method gives a stable performance estimate at low cost
  resamp_path <- glue("{exp_dir}/resamp_{test}.csv")
  if (!file.exists(resamp_path) || FORCE) {

    report(glue("{test}: Which resampling method gives a stable performance estimate?"))
    resamps <- list(
                 makeResampleDesc("Subsample", iters = 20, split = 0.3, blocking.cv = FALSE),
                 makeResampleDesc("Subsample", iters = 20, split = 0.3, blocking.cv = TRUE),
                 makeResampleDesc("Subsample", iters = 20, split = 0.5, blocking.cv = FALSE),
                 makeResampleDesc("Subsample", iters = 20, split = 0.5, blocking.cv = TRUE),
                 makeResampleDesc("RepCV",     reps = 4,   folds = 5,   blocking.cv = FALSE),
                 makeResampleDesc("RepCV",     reps = 4,   folds = 5,   blocking.cv = TRUE)
               )
    params <- list(mtry = 4, min.node.size = 50, sample.fraction = 0.63)
    res <- lapply(resamps, function(rd) {

             if (rd$id == "subsampling") {
               rd$desc <- glue("sub{rd$iters / 4}x{rd$split}x4")
             } else {
               rd$desc <- glue("cv{rd$folds}x4")
             }

             report(glue("{test} {rd$desc}"), VERBOSE)
             report(as.data.frame(params), VERBOSE)
             res <- mlr::resample(
                      learner = setHyperPars(learner, par.vals = params),
                      task = task,
                      resampling = rd,
                      measures = measures,
                      keep.pred = FALSE,
                      show.info = VERBOSE
                    )
             cv <- res$measures.test
             ref <- purrr::map(1:nrow(cv), function(i) {
                      as.data.frame(
                        c(resamp = rd$desc, blk = rd$blocking.cv, params),
                        stringsAsFactors = FALSE
                      )
                    }) %>%
                    bind_rows()
             ref$rep <- letters[ceiling(cv$iter / 5)]
             cv <- cbind(ref, cv)

             # Save
             path <- if_else(rd$blocking.cv, "blk", "") %>%
                     glue("{exp_dir}/resamp_{test}_{rd$desc}{blk}.csv", blk = .)
             fwrite(cv, path)
             report(glue("-> {path}"), VERBOSE)

             # Return
             cv

           }) %>%
           bind_rows()

    # Save combined table
    fwrite(res, resamp_path)
    report(glue("-> {resamp_path}"), VERBOSE)

    # Save plot
    p <- ggplot(res, aes(x = iter, y = rsq, colour = rep)) +
           geom_point() +
           stat_smooth(method = "lm", formula = y ~ 1) +
           facet_wrap(blk ~ resamp) +
           ggtitle(glue("{test} resamp stability"))
    path <- glue("{exp_dir}/resamp_{test}.png")
    ggsave(path, p)
    report(glue("-> {path}"), VERBOSE)

  }



  # Define tuning
  resamp <- makeResampleDesc("Subsample", iters = 5, split = 0.3, blocking.cv = FALSE)
  resamp$desc <- "sub5x0.3"
  params <- makeParamSet(
              makeIntegerParam("mtry", 1, getTaskNFeats(task)),
              makeIntegerParam("min.node.size", 10, 50)
            )
  ctrl <- mlr::makeTuneControlMBO(budget = 15, mbo.design = generateDesign(n = 10, params))



  # 2. How does tuning mtry and min.node.size affect performance?
  #    Do not tune sample.fraction b/c we tune using a small subsample of the data, so a large
  #    sample.fraction might not lead to overfitting when tuning but could produce overfitting when
  #    applied to a large sample of the dataset (during CV or final model selection)
  tune_path <- glue("{exp_dir}/tune_{test}_{resamp$desc}_hped.rds")
  if (!file.exists(resamp_path) || FORCE) {

    report(glue("{test} How does tuning affect performance"))
    report(glue("{test} {resamp$desc}"))
    tuned <- withCallingHandlers(
               mlr::tuneParams(
                 learner = learner,
                 task = task,
                 resampling = resamp,
                 measures = measures,
                 par.set = params,
                 control = ctrl,
                 show.info = VERBOSE
               ),
               warning = muffle_mlrMBO_tune_warning
             )
    hped <- mlr::generateHyperParsEffectData(tuned, partial.dep = TRUE)
    saveRDS(hped, tune_path)
    report(glue("-> {tune_path}"), VERBOSE)

    # mtry effect on rsq
    p <- plotHyperParsEffect(hped, "mtry", "rsq.test.mean", partial.dep.learn = "regr.ranger") +
           ggtitle(glue("{test} tune mtry {resamp$desc}"))
    path <- glue("{exp_dir}/tune_{test}_{resamp$desc}_mtry.png")
    ggsave(path, p)

    # min.node.size effect on rsq
    p <- plotHyperParsEffect(hped, "min.node.size", "rsq.test.mean", partial.dep.learn = "regr.ranger") +
           ggtitle(glue("{test} tune min.node.size {resamp$desc}"))
    path <- glue("{exp_dir}/tune_{test}_{resamp$desc}_nodesize.png")
    ggsave(path, p)

    # min.node.size effect on rsq
    p <- ggplot(hped$data) +
           geom_point(aes(x = min.node.size, y = rsq.test.mean, colour = as.factor(mtry))) +
           geom_line(aes(x = min.node.size, y = rsq.test.mean, colour = as.factor(mtry))) +
           ggtitle(glue("{test} tune node size vs rsq {resamp$desc}"))
    path <- glue("{exp_dir}/tune_{test}_{resamp$desc}_expers.png")
    ggsave(path, p)

    # min.node.size effect on time
    p <- ggplot(hped$data) +
           geom_point(aes(x = min.node.size, y = exec.time, colour = as.factor(mtry))) +
           geom_line(aes(x = min.node.size, y = exec.time, colour = as.factor(mtry))) +
           ggtitle(glue("{test} tune node size vs time {resamp$desc}"))
    path <- glue("{exp_dir}/tune_{test}_{resamp$desc}_time.png")
    ggsave(path, p)

  }


  # 3. Does tuning result change if CV blocking is used?
  resamp$blocking.cv <- TRUE
  resamp$desc <- "sub5x0.3blk"
  tune_path <- glue("{exp_dir}/tune_{test}_{resamp$desc}_hped.rds")
  if (!file.exists(resamp_path) || FORCE) {

    report(glue("{test} How does blocking change tuning result?"))

    report(glue("{test} {resamp$desc}"), VERBOSE)
    tuned <- withCallingHandlers(
               mlr::tuneParams(
                 learner = learner,
                 task = task,
                 resampling = resamp,
                 measures = measures,
                 par.set = params,
                 control = ctrl,
                 show.info = VERBOSE
               ),
               warning = muffle_mlrMBO_tune_warning
             )
    hped <- mlr::generateHyperParsEffectData(tuned, partial.dep = TRUE)
    saveRDS(hped, tune_path)
    report(glue("-> {tune_path}"), VERBOSE)

    # mtry effect on rsq
    p <- plotHyperParsEffect(hped, "mtry", "rsq.test.mean", partial.dep.learn = "regr.ranger") +
           ggtitle(glue("{test} tune mtry {resamp$desc}"))
    path <- glue("{exp_dir}/tune_{test}_{resamp$desc}_mtry.png")
    ggsave(path, p)

    # min.node.size effect on rsq
    p <- plotHyperParsEffect(hped, "min.node.size", "rsq.test.mean", partial.dep.learn = "regr.ranger") +
           ggtitle(glue("{test} tune min.node.size {resamp$desc}"))
    path <- glue("{exp_dir}/tune_{test}_{resamp$desc}_nodesize.png")
    ggsave(path, p)

    # min.node.size effect on rsq
    p <- ggplot(hped$data) +
           geom_point(aes(x = min.node.size, y = rsq.test.mean, colour = as.factor(mtry))) +
           geom_line(aes(x = min.node.size, y = rsq.test.mean, colour = as.factor(mtry))) +
           ggtitle(glue("{test} tune node size vs rsq {resamp$desc}"))
    path <- glue("{exp_dir}/tune_{test}_{resamp$desc}_expers.png")
    ggsave(path, p)

    # min.node.size effect on time
    p <- ggplot(hped$data) +
           geom_point(aes(x = min.node.size, y = exec.time, colour = as.factor(mtry))) +
           geom_line(aes(x = min.node.size, y = exec.time, colour = as.factor(mtry))) +
           ggtitle(glue("{test} tune node size vs time {resamp$desc}"))
    path <- glue("{exp_dir}/tune_{test}_{resamp$desc}_time.png")
    ggsave(path, p)

  }


  # 4. How much does mtry change CV performance
  cv_path <- glue("{exp_dir}/cv_{test}_mtry_1vs5.csv")
  if (!file.exists(resamp_path) || FORCE) {

    report(glue("{test} How does mtry change CV performance?"))

    resamp <- makeResampleDesc("RepCV", folds = 5, reps = 3, blocking.cv = TRUE)
    resamp$desc <- "rcv5x3blk"
    report(glue("{test} {resamp$desc} mtry 1 min.node.size 25"), VERBOSE)
    cv1 <- mlr::resample(
             learner = setHyperPars(learner, mtry = 1, min.node.size = 25),
             task = task,
             resampling = resamp,
             measures = measures,
             keep.pred = FALSE,
             show.info = VERBOSE
           )$measures.test
    cv1$mtry <- 1

    report(glue("{test} {resamp$desc} mtry 5 min.node.size 25"))
    cv5 <- mlr::resample(
             learner = setHyperPars(learner, mtry = 5, min.node.size = 25),
             task = task,
             resampling = resamp,
             measures = measures,
             keep.pred = FALSE,
             show.info = VERBOSE
           )$measures.test
    cv5$mtry <- 5
    cv <- rbind(cv1, cv5)
    cv$rep <- letters[ceiling(cv$iter / 5)]
    fwrite(cv, cv_path)
    report(glue("-> {cv_path}"), VERBOSE)

    p <- ggplot(cv, aes(x = iter, y = rsq, colour = rep, linetype = as.factor(mtry))) +
           geom_smooth(colour = "black", method = "lm", formula = y ~ 1, se = FALSE) +
           geom_point() +
           geom_line() +
           geom_smooth(method = "lm", formula = y ~ 1, se = FALSE)
    path <- glue("{exp_dir}/cv_{test}_mtry_1vs5.png")
    ggsave(path, p)
    report(glue("-> {path}"), VERBOSE)

  }

}


# More experiments -----

year = 2012
ncores = NCORES
verbose = T
force = F

# Load data and assign cv blocks
{
  db2_cc <- read_fst(glue("work/db/{year}/db2_{year}_cc.fst"), as.data.table = TRUE)
  blocks <- here(glue("work/db/{year}/db2_{year}_blocks.fst")) %>%
            read_fst(as.data.table = TRUE) %>%
            .[db2_cc, .(cv_block), on = .(date, sinu_id)]
  db2_cc[, cv_block := blocks]
  rm(blocks)
  names(db2_cc) %>% paste(collapse = " ")
}

# Define learner, resampling, and custom measures to monitor spatiotemporal CV
{
  feats <- c("sinu_x", "sinu_y", "dow", "doy", "aod469_utc00", "aod469_utc03",
             "aod469_utc06", "aod469_utc09", "aod469_utc12", "aod469_utc15",
             "aod469_utc18", "aod469_utc21")
  rf <- mlr::makeLearner(
          cl = "regr.ranger",
          num.trees = 96,
          replace = FALSE,
          num.threads = ncores
        )
  resamp <- mlr::makeResampleDesc(method = "CV", iters = 5, blocking.cv = TRUE)
              test_frac <- mlr::makeMeasure(
                 id = "test_frac",
                 minimize = FALSE,
                 properties = c("regr", "req.pred", "req.task"),
                 fun = function(task, model, pred, feats, extra.args) {
                         round(nrow(as.data.frame(pred)) / getTaskSize(task), 3)
                       }
               )
  test_frac <- mlr::makeMeasure(
                 id = "test_frac",
                 minimize = FALSE,
                 properties = c("regr", "req.pred", "req.task"),
                 fun = function(task, model, pred, feats, extra.args) {
                         round(nrow(as.data.frame(pred)) / getTaskSize(task), 3)
                       }
               )
  test_n <- mlr::makeMeasure(
              id = "test_n",
              minimize = FALSE,
              properties = c("regr", "req.pred", "req.task"),
              fun = function(task, model, pred, feats, extra.args) {
                      nrow(as.data.frame(pred))
                    }
            )
}

# How much does calculating feature importance slow CV?
# * feature importance in resample ~ doubles time

# Does tuning with a 50% sample find the optimal mtry? Should we tune by R2 or MAE?
# * Feb 2012
#   - mtry 1 -> R2 0.7104  MAE 0.03037
#   - mtry 2 -> R2 0.7077  MAE 0.03027
#   - mtry 3 -> R2 0.7069  MAE 0.03019
#   - mtry 4 -> R2 0.7057  MAE 0.03019 [lowest MAE]
#   - mtry 5 -> R2 0.7059  MAE 0.03016
# * Apr 2012 3/4/5 seem very similar
#   - mtry 3 -> R2 0.7968
#   - mtry 4 -> R2 0.7976 [lowest MAE]
#   - mtry 5 -> R2 0.7917 (old 0.7922)
#   - mtry 6 -> R2 0.7929
# * Jun 2012 may be better with mtry = 3 (tune on MAE?)
#   - mtry 1 -> R2 0.8210 (old 0.8137)
#   - mtry 3 -> R2 0.8205 [lowest MAE]
#   - mtry 4 -> R2 0.8183
#   - mtry 5 -> R2 0.8178
# * Sep 2012 1/2 seem very similar (tune on MAE?)
#   - mtry 1 -> R2 0.8600  MAE 0.0288
#   - mtry 2 -> R2 0.8581  MAE 0.0287 (old R2 0.8561)
#   - mtry 3 -> R2 0.8555  MAE 0.0290
#   - mtry 4 -> R2 0.8545  MAE 0.0290
#   - mtry 5 -> R2 0.8532  MAE 0.0291
#   - mtry 6 -> R2 0.8515  MAE 0.0292
#   - mtry 7 -> R2 0.8494  MAE 0.0293
{
  day = make_date(2012, 2, 1)
  report(glue("{format(day, '%Y-%m')} checking mtry"))
  dat <- db2_cc[month(date) == month(day), ]
  task <- mlr::makeRegrTask(
            data = dat[, c("aod_047", feats), with = FALSE] %>%
                   as.data.frame(),
            target = "aod_047",
            blocking = dat$cv_block
          )
  set.seed(100)
  resamp_inst <- makeResampleInstance(desc = resamp, task = task)
  for (mtry in 1:5) {
    report(glue("mtry: {mtry}"))
    assign(
      glue("cv_{format(day, '%m')}_mtry_{mtry}"),
      mlr::resample(
        learner = setHyperPars(rf, mtry = mtry),
        task = task,
        resampling = resamp_inst,
        measures = list(mlr::mae, mlr::rsq, mlr::rmse, test_frac, mlr::timetrain),
        show.info = verbose
      )
    )
  }

  day = make_date(2012, 4, 1)
  report(glue("{format(day, '%Y-%m')} checking mtry"))
  dat <- db2_cc[month(date) == month(day), ]
  task <- mlr::makeRegrTask(
            data = dat[, c("aod_047", feats), with = FALSE] %>%
                   as.data.frame(),
            target = "aod_047",
            blocking = dat$cv_block
          )
  set.seed(100)
  resamp_inst <- makeResampleInstance(desc = resamp, task = task)
  for (mtry in 3:6) {
    report(glue("mtry: {mtry}"))
    assign(
      glue("cv_{format(day, '%m')}_mtry_{mtry}"),
      mlr::resample(
        learner = setHyperPars(rf, mtry = mtry),
        task = task,
        resampling = resamp_inst,
        measures = list(mlr::rsq, mlr::mae, mlr::rmse, test_frac, mlr::timetrain),
        show.info = verbose
      )
    )
  }

  day = make_date(2012, 6, 1)
  report(glue("{format(day, '%Y-%m')} checking mtry"))
  dat <- db2_cc[month(date) == month(day), ]
  task <- mlr::makeRegrTask(
            data = dat[, c("aod_047", feats), with = FALSE] %>%
                   as.data.frame(),
            target = "aod_047",
            blocking = dat$cv_block
          )
  set.seed(100)
  resamp_inst <- makeResampleInstance(desc = resamp, task = task)
  for (mtry in c(1, 3, 4, 5)) {
    report(glue("mtry: {mtry}"))
    assign(
      glue("cv_{format(day, '%m')}_mtry_{mtry}"),
      mlr::resample(
        learner = setHyperPars(rf, mtry = mtry),
        task = task,
        resampling = resamp_inst,
        measures = list(mlr::rsq, mlr::mae, mlr::rmse, test_frac, mlr::timetrain),
        show.info = verbose
      )
    )
  }

  day = make_date(2012, 9, 1)
  report(glue("{format(day, '%Y-%m')} checking mtry"))
  dat <- db2_cc[month(date) == month(day), ]
  task <- mlr::makeRegrTask(
            data = dat[, c("aod_047", feats), with = FALSE] %>%
                   as.data.frame(),
            target = "aod_047",
            blocking = dat$cv_block
          )
  set.seed(100)
  resamp_inst <- makeResampleInstance(desc = resamp, task = task)
  for (mtry in 1:7) {
    report(glue("mtry: {mtry}"))
    assign(
      glue("cv_{format(day, '%m')}_mtry_{mtry}"),
      mlr::resample(
        learner = setHyperPars(rf, mtry = mtry),
        task = task,
        resampling = resamp_inst,
        measures = list(mlr::rsq, mlr::mae, mlr::rmse, test_frac, mlr::timetrain),
        show.info = verbose
      )
    )
  }
}


# Does increasing num.trees help for months with poor performance?
# * Dec 2012: num.trees 96 -> 150
#   - changes tune result (mtry 8; was 1)
#   - does not improve performance (R2 (best  for mtry ); was 0.4718 for mtry 1)
{
  day = make_date(2012, 12, 1)
  report(glue("{format(day, '%Y-%m')} testing 150 trees"))
  dat <- db2_cc[month(date) == month(day), ]
  task <- mlr::makeRegrTask(
            data = dat[, c("aod_047", feats), with = FALSE] %>%
                   as.data.frame(),
            target = "aod_047",
            blocking = dat$cv_block
          )
  report("tuning")
  set.seed(100)
  tuned <- withCallingHandlers(
             mlr::tuneParams(
               learner = setHyperPars(rf, num.trees = 150),
               task = mlr::downsample(task, perc = 0.5),
               resampling = resamp,
               measures = list(mlr::rsq, mlr::mae, mlr::rmse, test_n),
               par.set = makeParamSet(
                           makeIntegerParam("mtry", 1, mlr::getTaskNFeats(task))
                         ),
               control = mlr::makeTuneControlMBO(
                           budget = 7,
                           mbo.design = data.frame(mtry = 1:4)
                         ),
               show.info = verbose
             ),
             warning = muffle_mlrMBO_tune_warning
           )
  report("cross-validating")
  set.seed(100)
  cv <- mlr::resample(
          learner = mlr::setHyperPars(rf, par.vals = tuned$x),
          task = task,
          resampling = resamp,
          measures = list(mlr::rsq, mlr::mae, mlr::rmse, test_frac, mlr::timetrain),
          extract = mlr::getFeatureImportance,
          show.info = verbose
        )
}


# Does adding data from neighbouring years help for months with poor performance?
# Add Jan 2011 and 2013 to Jan 2012, include year as feature
# * does not change R2 tune result (mtry 1 vs 1) or MAE tune result (1 vs 1)
# * does not improve performance
#   - mtry 1 -> R2 0.5273 (0.5243 old), MAE 0.02818 (0.02630 old)
#   - mtry 5 -> R2 0.5053 (0.4754 old), MAE 0.02849 (0.02644 old)
#   - mtry 6 -> R2 0.5009 (0.4705 old), MAE 0.02854 (0.02653 old)
#   - mtry 7 -> R2 0.5036 (       old), MAE 0.02845 (0.02661 old)
# Add Jan 2011 and 2013, do NOT include year as feature
# * MAE tune result may change to 6 (turns out to be non-optimal); R2 tune result still 1
# * does not substantially improve performance
#   - mtry 1 -> R2 0.5084 (0.5243 old), MAE 0.02905 (0.02630 old)
#   - mtry 5 -> R2 0.4906 (0.4754 old), MAE 0.02905 (0.02644 old)
#   - mtry 6 -> R2 0.4882 (0.4705 old), MAE 0.02906 (0.02653 old)
#   - mtry 7 -> R2 0.4899 (0.4556 old), MAE 0.02904 (0.02661 old)
#   - mtry 8 -> R2 0.4842 (0.4562 old), MAE 0.02892 (0.02653 old)
{
  day = make_date(2012, 1, 1)
  report(glue("{format(day, '%Y-%m')} testing with extra data from Jan 2011 and 2013"))
  dat <- db2_cc[month(date) == month(day), ]
  low <- dat[, .(.N, cv_block = first(cv_block)), by = .(sinu_id)] %>%
         .[N < 6, .(sinu_id, cv_block = as.integer(str_sub(cv_block, 6, 8)))]
  extra <- rbind(
             read_fst("work/db/2011/db2_2011-01.fst", as.data.table = T)[!is.na(aod_047), ],
             read_fst("work/db/2013/db2_2013-01.fst", as.data.table = T)[!is.na(aod_047), ]
           ) %>%
           setkeyv("sinu_id") %>%
           .[low, on = .(sinu_id)] %>%
           .[!is.na(date), ]
  extra[, cv_block := 100*as.integer(date) + cv_block]
  dat %<>% rbind(extra) %>%
           setkeyv(c("date", "sinu_id"))
  rm(low, extra)
  dat[, year := year(date)]
  task <- mlr::makeRegrTask(
            data = dat[, c("aod_047", feats, "year"), with = FALSE] %>%
                   as.data.frame(),
            target = "aod_047",
            blocking = dat$cv_block
          )
  report("tuning")
  set.seed(100)
  tuned <- withCallingHandlers(
             mlr::tuneParams(
               learner = rf,
               task = mlr::downsample(task, perc = 0.5),
               resampling = resamp,
               measures = list(mlr::mae, mlr::rsq, mlr::rmse, test_n),
               par.set = makeParamSet(
                           makeIntegerParam("mtry", 1, mlr::getTaskNFeats(task))
                         ),
               control = mlr::makeTuneControlMBO(
                           budget = 7,
                           mbo.design = data.frame(mtry = 1:4)
                         ),
               show.info = verbose
             ),
             warning = muffle_mlrMBO_tune_warning
           )
  report("cross-validating")
  for (mtry in 1:7) {
    report(glue("mtry: {mtry}"))
    set.seed(100)
    cv <- mlr::resample(
            learner = mlr::setHyperPars(rf, mtry = mtry),
            task = task,
            resampling = resamp,
            measures = list(mlr::rsq, mlr::mae, mlr::rmse, test_frac, mlr::timetrain),
            show.info = verbose
          )
  }
}
