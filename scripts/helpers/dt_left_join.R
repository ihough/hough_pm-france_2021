# Fast left join for keyed data.tables
# * Adds columns by reference if key columns match (fast and memory-efficient)
# * Raises if any columns to be added would overwrite existing columns
dt_left_join <- function(left, right) {

  # Ensure left and right are data.tables and left is keyed
  stopifnot(is.data.table(left))
  stopifnot(is.data.table(right))
  stopifnot(!is.null(key(left)))

  # Ensure right will not overwrite existing columns of left
  cols_to_join <- setdiff(names(right), key(left))
  collisions <- intersect(cols_to_join, names(left))
  if (length(collisions) > 0) {
    collisions <- paste(collisions, collapse = "' '")
    stop(paste0("Columns '", collisions, "' of right exist in left"))
  }

  # Prepare columns of right for joining to left
  if (identical(left[, key(left), with = FALSE], right[, key(right), with = FALSE])) {

    # Key cols match; take cols to join from right
    right <- right[, ..cols_to_join]

  } else {

    # Key cols not identical; join right to left and take cols to join
    join_on <- intersect(key(left), names(right))
    if (length(join_on) == 0) {
      stop("No keys of left exist in right")
    }
    right <- right[left, ..cols_to_join, on = join_on]

    # Confirm row counts match
    if (nrow(right) != nrow(left)) {
      stop("Row count mismatch. Did you mean to join columns from left onto right?")
    }

  }

  # Add columns from right to left by reference
  left[, (cols_to_join) := right]

  invisible(left)

}
