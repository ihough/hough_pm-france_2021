# Create an n-fold nested cross-validation scheme to assess base learner and ensemble performance
# -> list of length n * (n - 1) / 2
# -> each item is a list of two items:
#    * test = list of (2) IDs identifying the test folds
#    * train = list of (n - 2) IDs identifying the train folds
#
# To use this scheme you must first assign each row (observation) to one of n folds. The fold
# assignments must be *fixed* across all base learners and the ensemble; otherwise the ensemble may
# be biased because some learners will have been trained on data that was withheld from others.
#
# To assess the performance of a base learner:
# * For each item in the scheme:
#   - Reserve the 2 test folds of data
#   - Train the base learner on the (n - 2) remaining train folds
#   - Generate predictions for the 2 test folds
#   - Label each prediction with the ID of *both* test folds
# * Use the resulting (n - 1) out-of-sample predictions for each row to assess the base learner
#
# To assess the performance of the ensemble:
# * For each fold of data:
#   - Reserve the fold
#   - Train the ensemble on the predictions for the (n - 1) remaining folds that are labeled with
#     the ID of the reserved fold. These predictions were produced by a model did not have access
#   - Generate predictions for the reserved fold
# * This results in 1 out-of-sample prediction for each row. Use these to estimate the ensemble's
#   performance.
make_cv_scheme <- function(folds, as.data.table = FALSE) {

  stopifnot(is.numeric(folds))
  stopifnot(length(folds) == 1)
  stopifnot(as.integer(folds) > 2)

  folds <- seq_len(folds)
  scheme <- combn(folds, 2, simplify = FALSE, FUN = function(x) {
              list(
                test = x,
                train = setdiff(folds, x)
              )
            })

  if (isTRUE(as.data.table)) {

    return(cv_scheme_to_dt(scheme))

  } else {

    return(scheme)

  }

}

# Convert a cross-validation scheme to a data.table with two list columns, "test", and "train"
cv_scheme_to_dt <- function(cv_scheme) {

  rbindlist(
    lapply(cv_scheme, function(x) {
      data.table(test = list(x$test), train = list(x$train))
    })
  )

}
