# Function to impute missing values in a raster via focal mean with Gaussian kernel
# Similar to inverse distance weighting, but faster if gaps are not too big
# * rst = raster layer with missing values (NA)
# * d = sigma for the Guassian kernel (see raster::focalWeight); setting sigma to slightly less than
#       the raster resolution produces a 5x5 weights matrix; sigma = resolution -> 7x7 matrix
# * maxiters = give up if values are still missing after this many iterations
impute_focal <- function(rst, d, maxiters = 10) {

  # Function to calculate focal mean excluding NA
  # Needed b/c raster::focal(fun = mean, na.rm = TRUE) produces incorrect results if weights matrix
  # is not all 1
  foc <- function(x, ...) {
    sum(x, na.rm = TRUE) / sum(wm[!is.na(x)])
  }

  # Define the weights matrix
  wm <- raster::focalWeight(rst, d = d, type = "Gauss")

  # Impute repeatedly until all NA filled or maxiters reached
  i <- 0
  repeat {

    check <- cellStats(is.na(rst), "sum")
    if (check == 0) {
      break
    } else if (i == maxiters) {
      stop(glue("maxiters reached: {check} cells are still NA after {i} rounds of imputation"))
    }

    rst <- raster::focal(rst, w = wm, fun = foc, na.rm = TRUE, pad = TRUE, NAonly = TRUE)
    i <- i + 1

  }

  return(rst)

}
